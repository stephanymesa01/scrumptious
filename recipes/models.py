from django.db import models
from django.conf import settings

class Recipe(models.Model):
    title = models.CharField(max_length=255)
    picture = models.URLField(null=True, blank=True)
    description = models.TextField()
    email_address = models.EmailField(max_length=300, null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Recipe"
        verbose_name_plural = "Recipes"

class RecipeStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(
        'Recipe',
        related_name="steps",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["step_number"]
        verbose_name = "Recipe Step"
        verbose_name_plural = "Recipe Steps"

class Ingredients(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.TextField(max_length=100)
    recipe = models.ForeignKey(
        'Recipe',
        related_name="ingredients",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["food_item"]
        verbose_name = "Recipe Ingredient"
        verbose_name_plural = "Recipe Ingredients"
