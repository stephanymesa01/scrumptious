from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden
from .models import Recipe, Ingredients, RecipeStep
from .forms import RecipeForm, IngredientsForm, RecipeStepForm
from django.forms import modelformset_factory

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {"recipe_object": recipe}
    return render(request, "recipes/detail.html", context)

@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {"recipe_list": recipes}
    return render(request, "recipes/list.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {"recipe_list": recipes}
    return render(request, "recipes/list.html", context)

@login_required
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(commit=False)
            recipe.author = request.user
            recipe.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm()

    context = {"form": form}
    return render(request, "recipes/create.html", context)

@login_required
def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)

    # Check if the current user is the owner of the recipe
    if request.user != recipe.author:
        return HttpResponseForbidden("You don't have permission to edit this recipe.")

    RecipeStepFormSet = modelformset_factory(RecipeStep, form=RecipeStepForm, extra=0, can_delete=True)
    IngredientFormSet = modelformset_factory(Ingredients, form=IngredientsForm, extra=0, can_delete=True)

    if request.method == 'POST':
        recipe_form = RecipeForm(request.POST, instance=recipe)
        recipestep_formset = RecipeStepFormSet(request.POST, queryset=RecipeStep.objects.filter(recipe=recipe), prefix='steps')
        ingredient_formset = IngredientFormSet(request.POST, queryset=Ingredients.objects.filter(recipe=recipe), prefix='ingredients')

        if recipe_form.is_valid() and recipestep_formset.is_valid() and ingredient_formset.is_valid():
            recipe_form.save()
            for form in recipestep_formset:
                if form.is_valid():
                    step_instance = form.save(commit=False)
                    step_instance.recipe = recipe
                    if form.cleaned_data.get('DELETE'):
                        step_instance.delete()
                    else:
                        step_instance.save()
            for form in ingredient_formset:
                if form.is_valid():
                    ingredient_instance = form.save(commit=False)
                    ingredient_instance.recipe = recipe
                    if form.cleaned_data.get('DELETE'):
                        ingredient_instance.delete()
                    else:
                        ingredient_instance.save()

            return redirect('show_recipe', id=recipe.id)
    else:
        recipe_form = RecipeForm(instance=recipe)
        recipestep_formset = RecipeStepFormSet(queryset=RecipeStep.objects.filter(recipe=recipe), prefix='steps')
        ingredient_formset = IngredientFormSet(queryset=Ingredients.objects.filter(recipe=recipe), prefix='ingredients')

    context = {
        'recipe': recipe,
        'recipe_form': recipe_form,
        'recipestep_formset': recipestep_formset,
        'ingredient_formset': ingredient_formset,
    }

    return render(request, 'recipes/edit.html', context)
