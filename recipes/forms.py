from django import forms
from django.forms import formset_factory
from .models import Recipe, RecipeStep, Ingredients  # Import Ingredients model

class RecipeForm(forms.ModelForm):
    email_address = forms.EmailField(max_length=300)

    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
            "email_address",
        ]

class RecipeStepForm(forms.ModelForm):
    class Meta:
        model = RecipeStep
        fields = ["instruction"]

class IngredientsForm(forms.ModelForm):
    class Meta:
        model = Ingredients
        fields = ["amount", "food_item"]

IngredientFormSet = formset_factory(IngredientsForm, extra=1)
