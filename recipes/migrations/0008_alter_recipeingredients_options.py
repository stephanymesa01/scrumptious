# Generated by Django 5.0.1 on 2024-01-25 20:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0007_recipeingredients'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='recipeingredients',
            options={'ordering': ['food_item'], 'verbose_name': 'Recipe Ingredient', 'verbose_name_plural': 'Recipe Ingredients'},
        ),
    ]
