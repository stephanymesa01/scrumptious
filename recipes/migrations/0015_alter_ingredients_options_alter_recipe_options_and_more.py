# Generated by Django 5.0.1 on 2024-01-26 18:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0014_recipe_author'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ingredients',
            options={'ordering': ['food_item'], 'verbose_name': 'Recipe Ingredient', 'verbose_name_plural': 'Recipe Ingredients'},
        ),
        migrations.AlterModelOptions(
            name='recipe',
            options={'verbose_name': 'Recipe', 'verbose_name_plural': 'Recipes'},
        ),
        migrations.AlterModelOptions(
            name='recipestep',
            options={'ordering': ['step_number'], 'verbose_name': 'Recipe Step', 'verbose_name_plural': 'Recipe Steps'},
        ),
    ]
